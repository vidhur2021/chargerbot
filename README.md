CHARGER BOT

A chatbot provides an interactive environment for students to get to know about university of new haven
• For simplicity, ability of charger bot’s intelligence is constrained to the locations of main buildings at UNH, courses offered and basic details related to University of New Haven.


Instruction to run:

Run the chatbot_training.py file to train the model.

Python chatbot_training.py

If it ran successfully. Run the gui

Python chatgui.py


Enjoy chatting with the chatbot.



Continuous updates will be made to the chatbots with new updates.

v 0.0.1 :
Chargerbot understands and answers basic questions regarding university of new haven
Chargerbot can answer questions regarding the courses available on Computers science field.
Chargerbot can answer questions regarding programs that are available in UNH

Future updates:
v 0.0.2:
ChargerBot can understand and answer questions in related to international students office. 
ChargerBot can understand and answer questions related to professors of Unh.