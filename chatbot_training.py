#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 15:08:08 2020

@author: vidhursavyasachin
"""

import nltk
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
import json
import pickle
import numpy as np
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import SGD
import random


letters=[]
key_names = []
documents = []
letters_to_ignore = ['?', '!']
def tokenize(pattern):
    return nltk.word_tokenize(pattern)

def load_json(data_file):
    intents = json.loads(data_file)
    return intents
def sorting_letters(letters):
    return sorted(list(set(letters)))

def sorting(ip_for_data):
    return sorted(list(set(ip_for_data)))
def pickle_dump(letters,key_names):
    pickle.dump(letters,open('letters.pkl','wb'))
    pickle.dump(key_names,open('key_names.pkl','wb'))
data_file = open('intents.json').read()
intents = load_json(data_file)

for intent in intents['intents']:
    for pattern in intent['questions']:
        #tokenize each word
        w = nltk.word_tokenize(pattern)
        letters.extend(w)
        #add documents in the corpus
        documents.append((w, intent['key']))

        # add to our key_names list
        if intent['key'] not in key_names:
            key_names.append(intent['key'])
letters = [lemmatizer.lemmatize(w.lower()) for w in letters if w not in letters_to_ignore]
letters = sorting(letters)
key_names = sorting(key_names)
pickle_dump(letters,key_names)
# create our training data
training = []
# create an empty array for our output
output_empty = [0] * len(key_names)
def shuffle(training_data):
    return random.shuffle(training_data)
for doc in documents:
    bag_of_words = []
    pattern_letters = doc[0]
    pattern_letters = [lemmatizer.lemmatize(word.lower()) for word in pattern_letters]
    for words in letters:
        bag_of_words.append(1) if words in pattern_letters else bag_of_words.append(0)
    output_row = list(output_empty)
    output_row[key_names.index(doc[1])] = 1
    training.append([bag_of_words, output_row])
shuffle(training)
training = np.array(training)
def model_design():
    x_train = list(training[:,0])
    y_train = list(training[:,1])
    print("Training data created")
    model = Sequential()
    model.add(Dense(128, input_shape=(len(x_train[0]),), activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(len(y_train[0]), activation='softmax'))
    # Compile model. Stochastic gradient descent with Nesterov accelerated gradient gives good results for this model
    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=['accuracy'])
    #fitting and saving the model 
    hist = model.fit(np.array(x_train), np.array(y_train), epochs=200, batch_size=5, verbose=1)
    model.save('chatbot_model.h5', hist)

model_design()



