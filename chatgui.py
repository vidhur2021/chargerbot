import nltk
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
import pickle
import numpy as np
from keras.models import load_model
model = load_model('chatbot_model.h5')
import json
import random
intents = json.loads(open('intents.json').read())
words = pickle.load(open('letters.pkl','rb'))
classes = pickle.load(open('key_names.pkl','rb'))



def tokenizing_sentences(sentence_to_words):
    return nltk.word_tokenize(sentence_to_words)
def lammatizing_words(words_tokenized):
    lemmatized_words = []
    for word in words_tokenized:
        lemmatized_words.append(lemmatizer.lemmatize(word.lower()))
    return lemmatized_words
def preprocessing_sentences(sentence_to_words,words_from_pickle):
    words_split = tokenizing_sentences(sentence_to_words)
    words_split = lammatizing_words(words_split)
    
    bag_of_words_init = [0] * len(words_from_pickle)
    for word_in_sentence in words_split:
        for index,word in enumerate(words_from_pickle):
            if word == word_in_sentence:
                bag_of_words_init[index] = 1
    return np.array(bag_of_words_init)
def prediction(sentence_to_predict,model_to_predict_from):
    predictions = preprocessing_sentences(sentence_to_predict,words)
    predictions_to_np = np.array([predictions])
    model_prediction = model_to_predict_from.predict(predictions_to_np)[0]
    prediction_results = []
    for index,result in enumerate(model_prediction):
        if result>0.25:
            prediction_results.append([index,result])
    prediction_results.sort(key=lambda x:x[1],reverse = True)
    return_list = []
    for r in prediction_results:
        return_list.append({"intent": classes[r[0]], "probability": str(r[1])})
    tags_json = return_list[0]['intent']
    intents_json = intents['intents']
    for i in intents_json:
        if(i['key']==tags_json):
            response = random.choice(i['answers'])
            break
    return response
def charget_bot_response(sentence_from_user):
    return prediction(sentence_from_user,model)

            



#Creating GUI with tkinter
import tkinter
from tkinter import *


def send():
    msg = EntryBox.get("1.0",'end-1c').strip()
    EntryBox.delete("0.0",END)

    if msg != '':
        ChatLog.config(state=NORMAL)
        ChatLog.insert(END, "You: " + msg + '\n\n')
        ChatLog.config(foreground="#442365", font=("Verdana", 12 ))
    
        res = charget_bot_response(msg)
        ChatLog.insert(END, "Bot: " + res + '\n\n')
            
        ChatLog.config(state=DISABLED)
        ChatLog.yview(END)
 

base = Tk()
base.title("Hello")
base.geometry("400x500")
base.resizable(width=FALSE, height=FALSE)

#Create Chat window
ChatLog = Text(base, bd=0, bg="white", height="8", width="50", font="Arial",)

ChatLog.config(state=DISABLED)

#Bind scrollbar to Chat window
scrollbar = Scrollbar(base, command=ChatLog.yview, cursor="heart")
ChatLog['yscrollcommand'] = scrollbar.set

#Create Button to send message
SendButton = Button(base, font=("Verdana",12,'bold'), text="Send", width="12", height=5,
                    bd=0, bg="#32de97", activebackground="#3c9d9b",fg='#ffffff',
                    command= send )

#Create the box to enter message
EntryBox = Text(base, bd=0, bg="yellow",width="29", height="5", font="Arial")
#EntryBox.bind("<Return>", send)


#Place all components on the screen
scrollbar.place(x=376,y=6, height=386)
ChatLog.place(x=6,y=6, height=386, width=370)
EntryBox.place(x=128, y=401, height=90, width=265)
SendButton.place(x=6, y=401, height=90)

base.mainloop()
